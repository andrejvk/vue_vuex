import _ from 'lodash';
import Vue from 'vue';
import Vuex from 'vuex';
import './index.scss';
import Mapbox from 'mapbox-gl-vue';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

// let app;
Vue.use(Vuex);

function ready() {

  let appDiv = document.createElement('div');
  appDiv.id = "container";
  document.body.appendChild(appDiv);
  appDiv.innerHTML =`<div id='map' class='map'></div> ` ;

  let CounterDiv = document.createElement('div');
  // CounterDiv.innerHTML = ` <input type="button" value='закрыть' >`;
  // CounterDiv.innerHTML = `{{ count_sost }} <input type="button" value='закрыть' @click="increment_id" > `;
  CounterDiv.innerHTML = `состояние : {{ count_sost }} <input type="button" v-bind:value="count_sost_value" @click="increment_id" > `;
  appDiv.appendChild(CounterDiv);

///////////////////////
  var state = {
    count: true,
    count_value: 'закрыть',
    countid: 0,
    myMap_arr: null,
    myMap: null
  }
///////////////////////
  var mutations = {
    loadMap (state, myMap) {
      state.myMap = myMap;
    },
    loadMap_arr (state, myMap_arr) {
      state.myMap_arr = myMap_arr;
    },
    incrementid: state => {
      // console.log(state.myMap);
      // console.log(state.count);
      // console.log(JSON.parse(JSON.stringify(state.myMap)));
      if (state.count == false) {
        state.count = true;
        state.count_value = 'закрыть';
        store.dispatch('loadMArker_true');
      } else {
        state.count = false;
        state.count_value = 'открыть';
        store.dispatch('loadMArker_false');
      };
    }
  }
///////////////////////
  var actions = {

    // при состоянии count = true добавляем маркер
    // пробегаем по массиву и смотрим каких не достает
    loadMArker_true (context) {
      // console.log(store.state.myMap);
      console.log('loadMArker_true',store.state.count);
      store.dispatch('addMarker');
      // let arr = JSON.parse(store.state.myMap_arr) ;
      // for (let key in arr) {
      //   let map_driver_id = arr[key].id ;
      //   let map_driver_array = arr[key].map_infDriver ;
      //   console.log('loadMap', map_driver_id);
      //   // drawMarker(myMap,map_driver_id,map_driver_array);
      // }
    },

    // при состоянии count = false удаляем маркер
    // пока что один
    loadMArker_false (context) {
      // console.log(store.state.myMap);
      // console.log(state.count);
      console.log('loadMArker_false',store.state.count);
      let id_map = 'маркер2';
      removeLayer_Source(id_map,store.state.myMap);
      function removeLayer_Source(id_map,map) {
        if (map.getLayer(id_map) !== undefined) {
          map.removeLayer(id_map);
          map.removeSource(id_map);
        }
      }
    },

    //отдельно вынесеное добавление маркеров
    addMarker (context) {
      let arr_m = JSON.parse(store.state.myMap_arr) ;
      let addMarker_map = store.state.myMap;

      for (let key in arr_m) {
        let map_driver_id = arr_m[key].id ;
        let map_driver_array = arr_m[key].map_infDriver ;
        if (addMarker_map.getLayer(map_driver_id) == undefined) {
          drawMarker(addMarker_map,map_driver_id,map_driver_array);
        }
      }

      function drawMarker(map,map_driver_id,map_driver_array){
        map.addLayer({
          'id': map_driver_id,
          'type': 'symbol',
          'source': {
            'type': 'geojson',
            'data': {
              'type': 'FeatureCollection',
              'features': [{
                'type': 'Feature',
                'geometry': {
                  'type': 'Point',
                  'coordinates': map_driver_array
                },
                'properties': {
                  'title': map_driver_id,
                  // 'title': 'Mapbox DC',
                  // 'icon': 'monument'
                }
              }]
            }
          },
          'layout': {
            'icon-image': '{icon}-15',
            'text-field': '{title}',
            // 'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
            // 'text-offset': [0, 0.6],
            // 'text-anchor': 'top'
          }
        });
      }
    },

    // загрузка данных в состояние
    loadData (context) {
      var map_driver_arrayzz = [
        {	map_infDriver: [37.636651, 55.764481] ,
          id: 'маркер1',
          info: 'marker4',
          kartink: '01-8192.jpg'},
        {	map_infDriver: [37.630031, 55.764818] ,
          id: 'маркер2',
          info: 'marker4',
          kartink: ''},
      ];
      context.commit('loadMap_arr', JSON.stringify(map_driver_arrayzz)); //mutations
    },

    loadMap (context) {
      mapboxgl.accessToken = 'pk.eyJ1IjoiYW5kcmVqayIsImEiOiJjamgxdDRvZ2kwNWJsMnFtajk2b3hsbTI2In0.fHZOuHHyhUjZ7uAfUnNcUg'
      var myMap = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v9',
        // hash: true,
        center: [37.636580, 55.765273],
        zoom: 16
      });
      let nav = new mapboxgl.NavigationControl({
        //showCompass: false,
      });
      myMap.addControl(nav, 'top-right');
      myMap.addControl(new mapboxgl.GeolocateControl({
        positionOptions: {
          enableHighAccuracy: true,
        },
        trackUserLocation: true,
      }));
      myMap.addControl(new mapboxgl.FullscreenControl());

      store.dispatch('loadData'); // загрузка данных

      // let map_sost = JSON.parse(JSON.stringify(myMap));
      // context.commit('loadMap', map_sost);
      // console.log('Map', JSON.stringify(myMap));
      // store.commit('loadMap', myMap);

      myMap.on('load', function () {
        store.dispatch('addMarker');
      });

      context.commit('loadMap', myMap);

    }
  }
///////////////////////
  // This is store!!!.
  var store = new Vuex.Store({
    state,
    mutations,
    actions
  })

///////////////////////
  const vm = new Vue({
  	el: '#container',
    data: {
    },
    store,
    mounted () {
      this.$store.dispatch('loadMap')
    },
    computed: {
      count_sost () {
    	    return store.state.count
      },
      count_sost_value () {
    	    return store.state.count_value
      },
    },
    methods: {
      increment_id () {
          store.commit('incrementid');
      },
    }

 })
///////////////////////
}

document.addEventListener("DOMContentLoaded", ready);
