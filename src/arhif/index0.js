import _ from 'lodash';
import Vue from 'vue';
import Vuex from 'vuex';
import './index.scss';
import Mapbox from 'mapbox-gl-vue';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

// let app;
Vue.use(Vuex);

function ready() {

  let appDiv = document.createElement('div');
  appDiv.id = "container";
  document.body.appendChild(appDiv);
  appDiv.innerHTML =`<div id='map' class='map'></div> ` ;

  let CounterDiv = document.createElement('div');
  // CounterDiv.innerHTML = ` <input type="button" value='закрыть' >`;
  CounterDiv.innerHTML = `{{ count }} <input type="button" value='закрыть' @click="increment_id" > `;
  appDiv.appendChild(CounterDiv);

///////////////////////
  var state = {
    count: true,
    countid: 0,
    myMap: null
  }
  // This is look like events.
  var mutations = {
    loadMap (state, myMap) {
      state.myMap = myMap;
      // JSON.parse(JSON.stringify(state.myMap));
      // console.log(state.myMap);
    },
    incrementid: state => {
      console.log(state.myMap);
      // console.log(JSON.parse(JSON.stringify(state.myMap)));
      if (state.count == false) {
        state.count = true;
        // console.log(state.count);
      } else {
        state.count = false;
        // console.log(state.count);
      };
    }

  }
  var actions = {
    loadMap (context) {
      mapboxgl.accessToken = 'pk.eyJ1IjoiYW5kcmVqayIsImEiOiJjamgxdDRvZ2kwNWJsMnFtajk2b3hsbTI2In0.fHZOuHHyhUjZ7uAfUnNcUg'
      var myMap = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v9',
        // hash: true,
        center: [37.636580, 55.765273],
        zoom: 16
      })
      let nav = new mapboxgl.NavigationControl({
        //showCompass: false,
      });
      myMap.addControl(nav, 'top-right');
      myMap.addControl(new mapboxgl.GeolocateControl({
        positionOptions: {
          enableHighAccuracy: true,
        },
        trackUserLocation: true,
      }));
      myMap.addControl(new mapboxgl.FullscreenControl());


      var map_driver_arrayzz = [
  			{	map_infDriver: [37.636651, 55.764481] ,
  				id: 'маркер1',
  				info: 'marker4',
  				kartink: '01-8192.jpg'},
  			{	map_infDriver: [37.630031, 55.764818] ,
  				id: 'маркер2',
  				info: 'marker4',
  				kartink: ''},
  		];
      // if (store.state.count == true) {
      ///////////////////////
      // console.log(store.state.count);
        // for (let key in map_driver_arrayzz) {
        //   let map_driver_id = map_driver_arrayzz[key].id ;
        //   let map_driver_array = map_driver_arrayzz[key].map_infDriver ;
        //   // drawMarker(map,map_driver_id,map_driver_array);
        // }
      ///////////////
      // } else {
      //   let id_map = 'маркер2';
      //   removeLayer_Source(id_map,map);
      // }
      let map_driver_array_ob = map_driver_arrayzz;
		  let arr_marker = [];
      Draw_Marker(map_driver_array_ob, myMap, arr_marker);


      function Draw_Marker(map_driver_array_ob0, map0, arr_marker0) {
        for (let key in map_driver_array_ob0) {
          if (map_driver_array_ob0.hasOwnProperty(key)) {
            let map_driver_array = map_driver_array_ob0[key].map_infDriver ;
            let map_driver_info = map_driver_array_ob0[key].info ;
            let map_driver_id = map_driver_array_ob0[key].id ;
            // let map_driver_kartink = map_driver_array_ob0[key].kartink ;
            let markerEl = document.createElement('div');
            let x = map_driver_info ;
            let markerdiv = '' ;
            let markercolor = '#FFFFFF' ;
            let markerbackground = '#FF8C00' ;
            // задаем стиль маркера
            switch (x) {
            case 'marker1':
              markerdiv = `<div>
               <span style="border-radius:50%; box-shadow: 0 0 10px rgba(0,0,0,0.5); padding: 5px"> Ф </span>
               </div>`;
              break;
            case 'marker4':
              markercolor = '#000000' ;
              markerbackground = '#00FFFF' ;
              markerdiv = `<div >
              <span style="border-radius:50%; box-shadow: 0 0 5px rgba(0,0,0,0.5); padding: 0px">
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
    width="32" height="32"
    viewBox="0 0 252 252"
    style="fill:#3498db;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,252v-252h252v252z" fill="none"></path><g id="original-icon" fill="#e67e22"><g id="surface1"><path d="M126,10.08c-63.96469,0 -115.92,51.95531 -115.92,115.92c0,63.96469 51.95531,115.92 115.92,115.92c63.96469,0 115.92,-51.95531 115.92,-115.92c0,-63.96469 -51.95531,-115.92 -115.92,-115.92zM126,20.16c58.51125,0 105.84,47.32875 105.84,105.84c0,58.51125 -47.32875,105.84 -105.84,105.84c-58.51125,0 -105.84,-47.32875 -105.84,-105.84c0,-58.51125 47.32875,-105.84 105.84,-105.84zM126,30.24c-52.82156,0 -95.76,42.93844 -95.76,95.76c0,52.82156 42.93844,95.76 95.76,95.76c52.82156,0 95.76,-42.93844 95.76,-95.76c0,-52.82156 -42.93844,-95.76 -95.76,-95.76zM126,40.32c2.26406,0 4.54781,0.13781 6.7725,0.315c-1.27969,2.50031 -2.57906,5.41406 -3.9375,9.2925c-3.48469,9.98156 -6.91031,24.94406 -7.7175,46.305c-0.53156,0.07875 -1.04344,0.19688 -1.575,0.315c-7.02844,-18.99844 -9.39094,-32.95687 -9.9225,-42.3675c-0.35437,-6.20156 0.09844,-10.00125 0.4725,-12.285c5.15813,-0.96469 10.47375,-1.575 15.9075,-1.575zM143.4825,42.0525c7.59938,1.575 14.70656,4.29188 21.42,7.7175c-2.14594,1.83094 -4.50844,4.03594 -7.245,7.0875c-7.04812,7.89469 -15.96656,20.27813 -24.885,39.69c-0.53156,-0.11812 -1.04344,-0.21656 -1.575,-0.315c0.7875,-20.17969 3.99656,-33.96094 7.0875,-42.84c2.08688,-5.96531 3.97688,-9.37125 5.1975,-11.34zM99.6975,44.415c-0.21656,2.81531 -0.39375,6.08344 -0.1575,10.2375c0.59063,10.53281 3.16969,25.71188 10.5525,45.675c0.01969,0.03938 -0.01969,0.11813 0,0.1575c-0.45281,0.27563 -0.98437,0.4725 -1.4175,0.7875c-13.72219,-14.86406 -21.28219,-26.97187 -25.3575,-35.4375c-2.63812,-5.45344 -3.85875,-9.05625 -4.41,-11.34c6.39844,-4.23281 13.32844,-7.67812 20.79,-10.08zM174.195,55.125c6.31969,4.31156 12.06844,9.46969 17.01,15.2775c-2.6775,0.86625 -5.70937,1.98844 -9.45,3.78c-9.48937,4.5675 -22.38469,12.95438 -37.9575,27.405c-0.45281,-0.33469 -0.945,-0.63 -1.4175,-0.945c8.44594,-18.32906 16.57688,-30.00375 22.8375,-37.0125c4.17375,-4.68562 7.10719,-7.14656 8.9775,-8.505zM70.4025,60.795c0.86625,2.6775 1.98844,5.70938 3.78,9.45c4.58719,9.52875 12.85594,22.44375 27.405,38.115c-0.33469,0.45281 -0.63,0.945 -0.945,1.4175c-18.40781,-8.46562 -29.98406,-16.71469 -37.0125,-22.995c-4.68562,-4.17375 -7.14656,-7.10719 -8.505,-8.9775c4.31156,-6.31969 9.46969,-12.06844 15.2775,-17.01zM197.505,78.9075c4.23281,6.39844 7.67813,13.32844 10.08,20.79c-2.81531,-0.21656 -6.08344,-0.39375 -10.2375,-0.1575c-10.53281,0.59063 -25.71187,3.16969 -45.675,10.5525c-0.29531,-0.4725 -0.63,-0.96469 -0.945,-1.4175c14.86406,-13.72219 26.97188,-21.28219 35.4375,-25.3575c5.45344,-2.63812 9.05625,-3.85875 11.34,-4.41zM49.77,87.0975c1.83094,2.14594 4.03594,4.50844 7.0875,7.245c7.89469,7.04813 20.27813,15.96656 39.69,24.885c-0.11812,0.53156 -0.21656,1.04344 -0.315,1.575c-20.17969,-0.7875 -33.96094,-3.99656 -42.84,-7.0875c-5.96531,-2.08687 -9.37125,-3.97687 -11.34,-5.1975c1.575,-7.59937 4.29188,-14.70656 7.7175,-21.42zM126,105.84c5.21719,0 9.98156,1.96875 13.545,5.1975l1.4175,1.4175c3.22875,3.56344 5.1975,8.32781 5.1975,13.545c0,5.21719 -1.96875,9.98156 -5.1975,13.545l-1.4175,1.4175c-1.65375,1.49625 -3.58312,2.73656 -5.67,3.6225c-2.42156,1.02375 -5.07937,1.575 -7.875,1.575c-11.20219,0 -20.16,-8.95781 -20.16,-20.16c0,-8.38687 5.04,-15.53344 12.285,-18.585c2.42156,-1.02375 5.07938,-1.575 7.875,-1.575zM204.75,109.4625c2.48063,0.07875 4.05563,0.41344 5.355,0.63c0.96469,5.15813 1.575,10.47375 1.575,15.9075c0,2.26406 -0.13781,4.54781 -0.315,6.7725c-2.50031,-1.27969 -5.41406,-2.57906 -9.2925,-3.9375c-9.98156,-3.48469 -24.94406,-6.91031 -46.305,-7.7175c-0.07875,-0.53156 -0.19687,-1.04344 -0.315,-1.575c18.99844,-7.02844 32.95688,-9.39094 42.3675,-9.9225c2.69719,-0.1575 5.02031,-0.21656 6.93,-0.1575zM40.635,119.2275c2.50031,1.27969 5.41406,2.57906 9.2925,3.9375c9.98156,3.48469 24.94406,6.91031 46.305,7.7175c0.07875,0.53156 0.19688,1.04344 0.315,1.575c-18.99844,7.02844 -32.95687,9.39094 -42.3675,9.9225c-6.20156,0.35438 -10.00125,-0.09844 -12.285,-0.4725c-0.96469,-5.15812 -1.575,-10.47375 -1.575,-15.9075c0,-2.26406 0.13781,-4.54781 0.315,-6.7725zM155.7675,131.1975c20.17969,0.7875 33.96094,3.99656 42.84,7.0875c5.96531,2.08688 9.37125,3.97688 11.34,5.1975c-1.575,7.59938 -4.29187,14.70656 -7.7175,21.42c-1.83094,-2.14594 -4.03594,-4.50844 -7.0875,-7.245c-7.89469,-7.04812 -20.27812,-15.96656 -39.69,-24.885c0.11813,-0.53156 0.21656,-1.04344 0.315,-1.575zM100.3275,141.9075c0.29531,0.4725 0.63,0.96469 0.945,1.4175c-14.86406,13.72219 -26.97187,21.28219 -35.4375,25.3575c-5.45344,2.63813 -9.05625,3.85875 -11.34,4.41c-4.23281,-6.39844 -7.67812,-13.32844 -10.08,-20.79c2.81531,0.21656 6.08344,0.39375 10.2375,0.1575c10.53281,-0.59062 25.71188,-3.16969 45.675,-10.5525zM151.3575,142.2225c18.40781,8.46563 29.98406,16.71469 37.0125,22.995c4.68563,4.17375 7.14656,7.10719 8.505,8.9775c-4.31156,6.31969 -9.46969,12.06844 -15.2775,17.01c-0.86625,-2.6775 -1.98844,-5.70937 -3.78,-9.45c-4.58719,-9.52875 -12.85594,-22.44375 -27.405,-38.115c0.33469,-0.45281 0.63,-0.945 0.945,-1.4175zM108.36,150.4125c0.45281,0.33469 0.945,0.63 1.4175,0.945c-8.46562,18.40781 -16.71469,29.98406 -22.995,37.0125c-4.17375,4.68563 -7.10719,7.14656 -8.9775,8.505c-6.31969,-4.31156 -12.06844,-9.46969 -17.01,-15.2775c2.6775,-0.86625 5.70938,-1.98844 9.45,-3.78c9.52875,-4.58719 22.44375,-12.85594 38.115,-27.405zM143.325,150.7275c13.72219,14.86406 21.28219,26.97188 25.3575,35.4375c2.63813,5.45344 3.85875,9.05625 4.41,11.34c-6.39844,4.23281 -13.32844,7.67813 -20.79,10.08c0.21656,-2.81531 0.39375,-6.08344 0.1575,-10.2375c-0.59062,-10.53281 -3.16969,-25.71187 -10.5525,-45.675c-0.01969,-0.03937 0.01969,-0.11812 0,-0.1575c0.45281,-0.27562 0.98438,-0.4725 1.4175,-0.7875zM119.2275,155.4525c0.53156,0.11813 1.04344,0.21656 1.575,0.315c-0.7875,20.17969 -3.99656,33.96094 -7.0875,42.84c-2.08687,5.96531 -3.97687,9.37125 -5.1975,11.34c-7.59937,-1.575 -14.70656,-4.29187 -21.42,-7.7175c2.14594,-1.83094 4.50844,-4.03594 7.245,-7.0875c7.04813,-7.89469 15.96656,-20.27812 24.885,-39.69zM132.4575,155.4525c7.02844,18.99844 9.39094,32.95688 9.9225,42.3675c0.35438,6.20156 -0.09844,10.00125 -0.4725,12.285c-5.15812,0.96469 -10.47375,1.575 -15.9075,1.575c-2.26406,0 -4.54781,-0.13781 -6.7725,-0.315c1.27969,-2.50031 2.57906,-5.41406 3.9375,-9.2925c3.48469,-9.98156 6.91031,-24.94406 7.7175,-46.305c0.53156,-0.07875 1.04344,-0.19687 1.575,-0.315z"></path></g></g></g></svg>
              </span>
              </div>`;
              break;
            default:
              console.log('нет стиля маркеров');
            }

            //markerEl.style.background = markerbackground ;
            //markerEl.style.opacity = '0.1' ;
            markerEl.style.color = markercolor ;
            markerEl.style.borderRadius = '50%';
            markerEl.innerHTML = markerdiv; // основной стиль значка
            // markerEl.style.cursor = 'pointer';
            let button_markerEl = document.createElement('button');
            button_markerEl.setAttribute('type','button');
            button_markerEl.style.border = 'none' ;
            button_markerEl.style.cursor = 'pointer' ;
            button_markerEl.style.overflow = 'hidden' ;
            button_markerEl.style.outline = 'none' ;
            button_markerEl.style.borderRadius = '50%';
            //при нажатии на маркер открывать панораму
            button_markerEl.onclick = function(){
              map0.flyTo({ center: map_driver_array, zoom: 18 });
              // panoram_all(map_driver_kartink);
            }
            button_markerEl.id = key;
            button_markerEl.appendChild(markerEl);
            // создаем маркер
            let marker = new mapboxgl.Marker(button_markerEl, { offset: [0, 0] })
            .setLngLat(map_driver_array);
            //.setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
            //.setHTML(div.innerHTML));
            marker.addTo(map0); // добавляем маркеры на карту
            arr_marker0.push(marker);// сохраняем для определения центра между маркерами
            //document.getElementById("container").value;


          }
        }
        // определяем центр между маркерами
        let center_of_markers = Center_Arr_Marker(arr_marker0);
        // marker.remove();
        // console.log(center_of_markers);
        // переводим камеру на центр между маркерами
        map0.flyTo({ center: center_of_markers, zoom: 16 });
      }

      function Center_Arr_Marker(arr_marker1) {
    	  // arr = []; // массив маркеров
    	  let arr = arr_marker1;
    	  let latMAX; let latMIN; let lngMAX; let lngMIN;
    	  for (let i in arr) {
      		if (arr.hasOwnProperty(i)) {
      			// console.log(arr[i]);
      		  let currLAT = arr[i].getLngLat().lat;
      		  let currLNG = arr[i].getLngLat().lng;
      		  if (parseInt(i, 10) === 0) {
        			latMAX = currLAT;
        			latMIN = currLAT;
        			lngMAX = currLNG;
        			lngMIN = currLNG;
      		  } else {
        			if (currLAT > latMAX) {
        			  latMAX = currLAT;
        			} else if (currLAT < latMIN) {
        			  latMIN = currLAT;
        			}
        			if (currLNG > lngMAX) {
        			  lngMAX = currLNG;
        			} else if (currLNG < lngMIN) {
        			  lngMIN = currLNG;
        			}
      		  }
      		}
    	  }
    	  let center = [];
    	  center[0] = (lngMAX + lngMIN) / 2;
    	  center[1] = (latMAX + latMIN) / 2;
    	  // console.log('center', center);
    	  return center;
    	  // map1.flyTo({ center: center, zoom: 16 });
    	}

      // let map_sost = JSON.parse(JSON.stringify(myMap));
      // context.commit('loadMap', map_sost);
      context.commit('loadMap', myMap);

    }
  }
  // This is store!!!.
  var store = new Vuex.Store({
    state,
    mutations,
    actions
  })
///////////////////////
  // let id_map = 'маркер2';
  // removeLayer_Source(id_map,map0);
  // function removeLayer_Source(id_map,map) {
  //   if (map.getLayer(id_map) !== undefined) {
  //     map.removeLayer(id_map);
  //     map.removeSource(id_map);
  //   }
  // }

  const vm = new Vue({
  	el: '#container',
    data: {
    },
    store,
    mounted () {
      this.$store.dispatch('loadMap')
    },
    computed: {
      count () {
    	    return store.state.count
      },


    },
    methods: {
      increment_id () {
          store.commit('incrementid');
      },
    }

 })

}

document.addEventListener("DOMContentLoaded", ready);
